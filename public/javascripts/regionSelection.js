/*
	This funtion handle the behavior of the selection of region
	All regions <p> have the regionClass and the selected <p>
	have the selectIdName id
	You can configure the colors (default,hover,selected)
*/

function regionSelection(){
	// The id for the selected region
	var selectIdName = "selectedRegion";
	var regionClass = "servRegion";

	// Colors
	var colorDefault = "white";
	var colorHover = "green";
	var colorSelected = "red";

	// The selected region (here default region)
	var $selected = setSelectedAndColor($("#"+selectIdName),colorSelected,"");

	// The list of region <p>
	var $arrayLabel = $("."+regionClass); 

	// For each region <p>
	$arrayLabel.each(function(i){
		
		// If clic on it
		$(this).click(function(){
			
			// Remove the previous selected region
			$selected.removeAttr('id');
			$selected.css('color','white');
			
			// Set the new selected region its color and add the id
			$selected = setSelectedAndColor($(this),colorSelected,selectIdName);
		});
		
		// Control the color when hover and when leaves
		$(this).hover(function(){
			// If it is not the selected item
			checkAndSetColor($(this),$selected,colorHover);
		},
		function(){
			// Same when leaving
			checkAndSetColor($(this),$selected,colorDefault);
		});			
	});	

}

// Set the color if the two text's objects are equals

function checkAndSetColor(object1, object2, color)
{
	if(object1.text() != object2.text())
	{
		object1.css('color',color);
	}

}

// Set the color to newTag and add the given idToAdd
// Return the newTag

function setSelectedAndColor(newTag , color, idToAdd)
{
	newTag.css('color',color);

	if(idToAdd.length)
		newTag.attr('id',idToAdd);
	return newTag;
}