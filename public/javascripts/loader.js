$(document).ready(function(){
	
	// Prevent from starting the loader more than once at a time
	var locked=false;

	// Get the dots (which are the loader)
	var $indicatorDots=$(".send-indicator-dot");

	// colors for the animated loader
	var hexArray = ['#38b074'];

	/*
		Stop animated loader
	*/

	window.stopLoading = function()
	{
		stopAnimation();
		locked = false;
		$(".send").css("visibility","hidden");
		$(".send-indicator").css("visibility","hidden");
		$(".send-indicator").stop();
		$("formSection").stop();		
		$(".send-indicator").animate({opacity:0} , 1000);
		$("#formSection").animate({ 'marginTop': '5%', opacity: 1 }, 500);
		
		// reposition all particles
		$indicatorDots.each(function(i){
			$(this).css('left','49%');
			$(this).css('top','35%');
		});			
	}

	/*
		Start animated loader
	*/

	window.startLoader = function(){
		if(locked) return;
		
		$("#labelNotFound").hide();
		$(".send").css("visibility","visible");
		$(".send-indicator").css("visibility","visible");
		$(".send-indicator").stop();
		$("formSection").stop();		
		
		$(".send-indicator").animate({opacity : 1} , 1000);
		$("#formSection").animate({ 'marginTop': '+15%', opacity: 0.5 }, 500);

		locked=true;
		
		$indicatorDots.each(function(i){
			startCircleAnim($(this),200,0.1*i,0.1,i/20 + 0.5);

			var randomColor = hexArray[Math.floor(Math.random() * hexArray.length)];			
			
			var scale = 0.5 + Math.random()*0.5;

		   $(this).css("background",randomColor);
		   $(this).css("-webkit-transform","scale("+scale+", "+scale+")");
		   $(this).css("transform","scale("+scale+", "+scale+")");		   
		});
	}

	/*
		Stop the animation
	*/
	
	function stopAnimation()
	{
		$indicatorDots.each(function(i){
			stopCircleAnim($(this),0.3);
		});
	}
	
	function setupCircle($obj){
		if(typeof($obj.data("circle"))=="undefined"){
			$obj.data("circle",{radius:0,angle:0});

			function updateCirclePos(){
				var circle=$obj.data("circle");
				TweenMax.set($obj,{
					x:Math.cos(circle.angle)*circle.radius,
					y:Math.sin(circle.angle)*circle.radius,
				})
				requestAnimationFrame(updateCirclePos);
			}
			updateCirclePos();
		}
	}

	function startCircleAnim($obj,radius,delay,startDuration,loopDuration){
		setupCircle($obj);
		$obj.data("circle").radius=0;
		$obj.data("circle").angle=0;
		TweenMax.to($obj.data("circle"),startDuration,{
			delay:delay,
			radius:radius,
			ease:Quad.easeInOut
		});
		TweenMax.to($obj.data("circle"),loopDuration,{
			delay:delay,
			angle:Math.PI*2,
			ease:Linear.easeNone,
			repeat:-1
		});
	}
	
	function stopCircleAnim($obj,duration){
		TweenMax.to($obj.data("circle"),duration,{
			radius:0,
			ease:Quad.easeInOut,
			onComplete:function(){
				TweenMax.killTweensOf($obj.data("circle"));
			}
		});
	}  
});