// set error msgs
var summonerNotFoundMsg = "Summoner not found !";
var inputIsEmpty = "Please enter a summoner name !";	


var app = getAngular();

app.controller('searchCtrl', ['$scope','$routeSegment','$http', '$q', function($scope, $routeSegment, $http, $q){

	// The method called when hitting enter
	// The following methods make the request to the server to check
	// summoner name validity.
	$scope.search = function(e)
	{
		if(e.keyCode == 13)
		{
			// Get the input
			var name = $("#summonerName").val();		   

			// If its empty
			if(name == "")
			{
				// show error msg
				$("#labelNotFound").text(inputIsEmpty);
				$("#labelNotFound").show("slide",1000);
				return false;		
			}		
		
			// Get the summoner name to lower case
			var region = $("#selectedRegion").text().toLocaleLowerCase();
			// Erase all spaces
			name.replace(" ","");

			// Start loader
			startLoader();

			// Start ajax request
			getSummoner('http://localhost:8080/api/summoner',name,region,$scope);
		}
	}


}]);

/*
	Ajax request to get the summoner base on its name and region
	
	It displays a loader when loading the request and stops it
	when done
	Show a label with error msg (not found or name not set)
*/

function getSummoner(baseUrl,name,region,scope)
{
	$.ajax({
		url: baseUrl,
		method: 'GET',
		dataType: 'json',
		data: { summonerName : name , region : region }
	})
	.then(
	    function success(response) {
	    	// Stop the loader
	        stopLoading();

	        // If we found a summoner
	        if(response.correct)
	        {
	        	// Set the summoner data to scope
	        	scope.summoner = response.response;
	        	scope.region = region;

	        	alert(scope.toSource());

	        	// Redirect to the summoner page overview
     			var redirectTo = "http://localhost:8080/#/home/summoner/"+region+"/"+name+"/overview";
     			window.location.href = redirectTo; 		       
	        }else {
	        	// If not found show error msg
				$("#labelNotFound").text(summonerNotFoundMsg); 	       		
        		$("#labelNotFound").show("slide",1000);			
	        }
	    }
	);	
}
