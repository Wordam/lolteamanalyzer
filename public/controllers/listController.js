// set error msgs
var summonerNotFoundMsg = "Summoner not found !";
var inputIsEmpty = "Please enter a summoner name !";	


var app = getAngular();

app.controller('listCtrl', ['$scope','$routeSegment','$http', '$q', function($scope, $routeSegment, $http, $q){

	getLists("toto");

}]);

function insertList(userName,listName,data)
{
	var baseUrl = getBaseUrl()+"api/list/insert";

	$.ajax({
		url: baseUrl,
		method: 'POST',
		data: { userName : userName , listName : listName , data : data }
	})
	.then(
	    function success(response) {
	    	alert("List inserted :"+response.toSource());
	    }
	);	
}

// Delete the selected list

function deleteList(userName,listName)
{
	var baseUrl = getBaseUrl()+"api/list/delete";

	$.ajax({
		url: baseUrl,
		type: 'DELETE',
		data: { userName : userName , listName : listName }
	})
	.then(
	    function success(response) {
	    	alert("List deleted : "+response.toSource());
	    }
	);	
}

function updateList(userName,listName,data)
{
	var baseUrl = getBaseUrl()+"api/list/update";

	$.ajax({
		url: baseUrl,
		type: 'PUT',
		data: { userName : userName , listName : listName }
	})
	.then(
	    function success(response) {
	    	//alert("List updated :"+response.toSource());
	    }
	);
}

// Get all lists from a user

function getLists(userName)
{
	var baseUrl = getBaseUrl()+"api/lists/get";

	$.ajax({
		url: baseUrl,
		method: 'GET',
		data: { userName : userName }
	})
	.then(
	    function success(response) {
	    	alert("Get lists :"+response.toSource());
	    	return response.response;
	    }
	);	
}

// Get a specific list from a user and a list name
function getList(userName,listName)
{
	var baseUrl = getBaseUrl()+"api/list/get";

	$.ajax({
		url: baseUrl,
		method: 'GET',
		data: { userName : userName , listName : listName }
	})
	.then(
	    function success(response) {
	    	alert("Get list : "+response.toSource());
	    }
	);
}