var app = angular.module('app', ['ngRoute', 'ngAnimate', 'route-segment', 'view-segment']);

var SERVER = "http://localhost:8080/";

var SEARCH = SERVER+"#/home/search";

var SA_CHAMPION_ICON = "http://ddragon.leagueoflegends.com/cdn/5.22.3/img/champion/";

var classForSelected = 'selectedItem';

var BASE_URL = "http://localhost:8080/";

function getAngular()
{
  return app;
}

function getBaseUrl()
{
  return BASE_URL;
}

app.config(function($routeSegmentProvider, $routeProvider) {
    
    // Configuring provider options
    
    $routeSegmentProvider.options.autoLoadTemplates = true;
    
    // Setting routes

    setRoutes($routeSegmentProvider, $routeProvider);  
                                
}) ;

function setRoutes($routeSegmentProvider, $routeProvider)
{
  $routeSegmentProvider

      .when('/home/search',          'home.search')
      .when('/home/summoner/:region/:name/overview',    'home.summoner.overview')
      .when('/home/summoner/:region/:name/team',      'home.summoner.team')
      .when('/home/summoner/:region/:name/team/:teamId', 'home.summoner.team.detail')
      .when('/home/list/:listName', 'home.list')
                      
      .segment('home', {
          templateUrl: 'views/home.html',
          controller: 'mainCtrl'})
          
      .within()
          
          .segment('search', {
              'default': true,
              templateUrl: 'views/home/search/search.html',
              controller: 'searchCtrl'
            })
              
          .segment('summoner', {
              templateUrl: 'views/home/summoner/summoner.html',
              controller: 'summonerCtrl'
              })
              
          .within()                 
              .segment('overview', {
                  templateUrl: 'views/home/summoner/overview.html'
                })
                  
              .segment('team', {
                  templateUrl: 'views/home/summoner/team.html'})
                  
              .within()
                  .segment('detail', {
                  templateUrl: 'views/home/summoner/team/details.html'})
              .up()
          .up()

          .segment('list', {
            templateUrl: 'views/home/list/index.html',
            controller:'listCtrl'
          });

                  
  $routeProvider.otherwise({redirectTo: '/home/search'});  
}

/*

  This is the main controller

*/

app.controller('mainCtrl', function($scope, $routeSegment) {
  $scope.$routeSegment = $routeSegment;
});

/*
  
  This is the controller for the summoner page

*/

/*
app.controller('SummonerCtrl', ['$scope','$routeSegment','$http', '$q', function($scope, $routeSegment, $http, $q){

  $scope.userName = {};

  // The first thing we need to do is to check if the summoner
  // really exists (if case someone change the segments value)
  // In this case we need to update displayed informations

  checkSummonerExists($scope,$routeSegment,$http,$q);

  if(!$scope.champArray)
    setChampArray($http,$scope,$q);

  $scope.getChampionIcon = function(id)
  {
    //SA_CHAMPION_ICON
    if($scope.champArray)
    {
      if(isNumeric(id))
      {
        var data = $scope.champArray.value.data.response.data;
        var urlToRet = SA_CHAMPION_ICON+data[id].key+".png";
        return urlToRet;
      }else
        return 'NaN';
    }

    return "[undefined]";
  }

  $scope.checkTeamId = function(teamId)
  {
    if(teamId == 100)
      return "Blue"
    else
      return "Red";
  }

  $scope.getOtherTeam = function(id)
  {
    var toRet = "side100";

    if(id == 100)
    {
      toRet = "side200";
    }

    return toRet;
  }

  $scope.getKda = function($stats)
  {
    return kda = (($stats.championsKilled+$stats.assists) / $stats.numDeaths).toFixed(1);
  }

  $scope.getItem = function($id)
  {
    var link = "http://ddragon.leagueoflegends.com/cdn/5.22.3/img/item/"+$id+".png";
    return link;
  }

  $scope.isValid = function(val)
  {
    if(val)
      return true;

    return false;
  }

  $scope.getWin = function(win)
  {
    if(win)
      return "Win";

    return "Loose";
  }

  $scope.limitDisplayRecentGame = 10;

  $scope.selectSingleGame = function(e)
  {
    var item = e.target;
    if($(item).hasClass('singleRecentGame'))
      toggle(item,classForSelected);
  }

  $scope.createListFromSelection = function()
  {
    // get selected elements
    var games = angular.element("."+classForSelected);

    var neededGamesData = { name : "name" };

    // get 
    var gamesData = $scope.recentGameData.games;

    // for each game container selected
    angular.forEach(games,function(game){
      // get the game (in jquery)
      var $game = $(game);

      // get the game id
      var gameId = $game.attr('id');

      // for each game data
      angular.forEach(gamesData, function(gameData){
        // should always find one game
        if(gameData.gameId == gameId)
        {
          // add this game data to an array
          neededGamesData[gameId] = gameData;
        }
      });
    });

    if(!$scope.gameLists)
      $scope.gameLists = { name : neededGamesData };
    else
      $scope.gameLists['name'] = neededGamesData;

    alert("Games data list "+$scope.gameLists.toSource());
  }

  $scope.renderList = function(listToRender)
  {
    alert("Redirect !");
    window.location = '/home/list';
  }

}]);

function setChampArray($http,$scope,$q) {  
  var deferred = $q.defer();
  var url_ = SERVER + "api/champion";

  // get the summoner information
  var promise = $http({
    method:'GET',
    url: url_
  });

  $scope.champArray = $q.when(promise).$$state;  
  $scope.$apply();
}

function getSummonerName(id,$scope,$http,$q)
{
  // http request to get summoner by id
  var url_ = SERVER + "api/summoner/id";

  // pass the id
  var params = {
    id : id
  };

  $http({
    method:'GET',
    url: url_,
    params : params
    }).then(function(result){
      $scope.userName[id] = result.data.response[id].name;   
    },function(error){
      alert("Error : "+error);
    });
}


function afterLoadedRecentGameData($scope,$http,$q)
{
  angular.forEach($scope.recentGameData.games, function(game) {
    angular.forEach(game.fellowPlayers, function(fellowPlayer){
      getSummonerName(fellowPlayer.summonerId,$scope,$http,$q);
    });
  });
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function checkSummonerExists($scope, $routeSegment, $http,$q)
{
  // build the url
  var url_ = SERVER+"api/summoner";

  // get the segment param (url based #/home/seg2/seg3/seg4)
  var params = $routeSegment.$routeParams;

  // get the summoner information
  $http({
    method:'GET',
    url: url_,
    params: { region : params.region , summonerName : params.name}
  }).then(function(response){
    // not found
    if(!response.data.correct)
    {
      var redirectTo = SEARCH;
      window.location.href = redirectTo;
    }else
    {
      // update summoner data
      $scope.summoner = response.data.response[params.name];
      $scope.currentRegion = params.region;

      // Update the list of recent games
      updateRecentGameData($scope,$routeSegment,$http,response.data,$q);
    }
  }, function(response){
    alert('Error : '+response);
    window.location.href = '/#/home/search';
  });
}

function updateRecentGameData($scope,$routeSegment,$http,data,$q)
{
  var summonerId = $scope.summoner.id;
  var region = $scope.currentRegion;

  var url_ = SERVER+"api/summoner/games";


  // get the summoner information
  $http({
    method:'GET',
    url: url_,
    params: { region : region , id : summonerId }
  }).then(function(response){
    // not found
    if(!response.data.correct)
    {
      alert("Uncorrect response : "+response.toSource());
      var redirectTo = "http://localhost:8080/#/home/search";
      window.location.href = redirectTo;
    }else
    {
      $scope.recentGameData = response.data.response;

      //alert("DATA : "+$scope.recentGameData.toSource());

      getSummonerName($scope.recentGameData.summonerId,$scope,$http,$q);

      afterLoadedRecentGameData($scope,$http,$q);
    }
  }, function(response){
    alert('Error (updateRecentGameData): '+response.toSource());
    window.location.href = '/#/home/search';
  });  
}

function toggle(element, klass) {
  var classes = element.className.match(/\S+/g) || [],
      index = classes.indexOf(klass);

  index >= 0 ? classes.splice(index, 1) : classes.push(klass);
  element.className = classes.join(' ');
}
*/