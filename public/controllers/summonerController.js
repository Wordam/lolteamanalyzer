var app = getAngular();

app.controller('summonerCtrl', ['$scope','$routeSegment','$http', '$q', function($scope, $routeSegment, $http, $q){

  $scope.userName = {};

  // The first thing we need to do is to check if the summoner is set
  // If not we need to check if it exists
  checkSummonerExists($scope,$routeSegment,$http,$q);

  // Init the index to display champion icones
  if(!$scope.champArray)
    setChampArray($http,$scope,$q);

  $scope.getChampionIcon = function(id)
  {
    //SA_CHAMPION_ICON
    if($scope.champArray)
    {
      if(isNumeric(id))
      {
        var data = $scope.champArray.value.data.response.data;
        var urlToRet = SA_CHAMPION_ICON+data[id].key+".png";
        return urlToRet;
      }else
        return 'NaN';
    }

    return "[undefined]";
  }

  $scope.checkTeamId = function(teamId)
  {
    if(teamId == 100)
      return "Blue"
    else
      return "Red";
  }

  $scope.getOtherTeam = function(id)
  {
    var toRet = "side100";

    if(id == 100)
    {
      toRet = "side200";
    }

    return toRet;
  }

  $scope.getKda = function($stats)
  {
    return kda = (($stats.championsKilled+$stats.assists) / $stats.numDeaths).toFixed(1);
  }

  $scope.getItem = function($id)
  {
    var link = "http://ddragon.leagueoflegends.com/cdn/5.22.3/img/item/"+$id+".png";
    return link;
  }

  $scope.isValid = function(val)
  {
    if(val)
      return true;

    return false;
  }

  $scope.getWin = function(win)
  {
    if(win)
      return "Win";

    return "Loose";
  }

  $scope.limitDisplayRecentGame = 10;

  $scope.selectSingleGame = function(e)
  {
    var item = e.target;
    if($(item).hasClass('singleRecentGame'))
      toggle(item,classForSelected);
  }

  $scope.createListFromSelection = function()
  {
    // get selected elements
    var games = angular.element("."+classForSelected);

    var listName = "listName1";

    var neededGamesData = { name : listName , games : []};

    // get 
    var gamesData = $scope.recentGameData.games;

    // for each game container selected
    angular.forEach(games,function(game){
      // get the game (in jquery)
      var $game = $(game);

      // get the game id
      var gameId = $game.attr('id');

      // for each game data
      angular.forEach(gamesData, function(gameData){
        // should always find one game
        if(gameData.gameId == gameId)
        {
          delete gameData['$$hashKey'];
          // add this game data to an array
          neededGamesData.games.push(gameData);
        }
      });
    });

    // insert list to db
    alert(""+neededGamesData.toSource());
    insertList("toto",listName,angular.toJson(neededGamesData));
  }

  $scope.renderList = function(listToRender)
  {
    alert("Redirect !");
    window.location = '/home/list';
  }

}]);

/*

*/
function setChampArray($http,$scope,$q) {  
  var deferred = $q.defer();
  var url_ = getBaseUrl() + "api/champion";

  // get the summoner information
  var promise = $http({
    method:'GET',
    url: url_
  });

  $scope.champArray = $q.when(promise).$$state;  
  $scope.$apply();
}

/*

*/

function getSummonerName(id,$scope,$http)
{
  // http request to get summoner by id
  var url_ = getBaseUrl() + "api/summoner/id";

  // pass the id
  var params = {
    id : id
  };

  $http({
    method:'GET',
    url: url_,
    params : params
    }).then(function(result){
      $scope.userName[id] = result.data.response[id].name;   
    },function(error){
      alert("Error : "+error);
    });
}

/*

*/

function afterLoadedRecentGameData($scope,$http)
{
  angular.forEach($scope.recentGameData.games, function(game) {
    angular.forEach(game.fellowPlayers, function(fellowPlayer){
      getSummonerName(fellowPlayer.summonerId,$scope,$http);
    });
  });
}


/*

*/

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

/*

*/

function checkSummonerExists($scope, $routeSegment, $http,$q)
{
	// build the url
	var url_ = getBaseUrl()+"api/summoner";

	// get the segment param (url based #/home/seg2/seg3/seg4)
	var params = $routeSegment.$routeParams;

	// get the summoner information
	$http({
		method:'GET',
		url: url_,
		params: { region : params.region , summonerName : params.name}
	}).then(function(response){
		// not found
		if(!response.data.correct)
		{
	  		var redirectTo = SEARCH;
	  		window.location.href = redirectTo;
		}else
		{
	  		// update summoner data
	  		$scope.summoner = response.data.response[params.name];
	  		$scope.currentRegion = params.region;

			// Update the list of recent games
	  		updateRecentGameData($scope,$http,$q);
		}
	}, function(response){
		alert('Error : '+response);
		window.location.href = '/#/home/search';
	});
}

/*

*/

function updateRecentGameData($scope,$http,$q)
{
  var summonerId = $scope.summoner.id;
  var region = $scope.currentRegion;

  var url_ = getBaseUrl()+"api/summoner/games";


  // get the summoner information
  $http({
    method:'GET',
    url: url_,
    params: { region : region , id : summonerId }
  }).then(function(response){
    // not found
    if(!response.data.correct)
    {
      alert("Uncorrect response : "+response.toSource());
      var redirectTo = "http://localhost:8080/#/home/search";
      window.location.href = redirectTo;
    }else
    {
      $scope.recentGameData = response.data.response;

      getSummonerName($scope.recentGameData.summonerId,$scope,$http,$q);

      afterLoadedRecentGameData($scope,$http,$q);
    }
  }, function(response){
    alert('Error (updateRecentGameData): '+response.toSource());
    window.location.href = '/#/home/search';
  });  
}

/*

*/

function toggle(element, klass) {
  var classes = element.className.match(/\S+/g) || [],
      index = classes.indexOf(klass);

  index >= 0 ? classes.splice(index, 1) : classes.push(klass);
  element.className = classes.join(' ');
}