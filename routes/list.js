// Export the function to the app

module.exports = function(app,lolapi,db) {

	// Try to get all lists from a user

	app.get('/api/lists/get', function(req, res) {
		console.log("Get all lists");

		var userName = req.param("userName");

		if(checkParam(res,userName,"Username not set")) return;
		
	    // Get the listCollection
	    var collection = db.get('listCollection');

	    // Find all lists
	    collection.find({ userName : userName },{},function(e,data){
	    	if(e)
	    		 console.log("Error ! ",e);
   			sendResult(res,data);
    	});
	});

	// Try to get a list named listName associate with the
	// user named userName

	app.get('/api/list/get', function(req, res){
		console.log("Get list");

		var userName = req.param("userName");
		var listName = req.param("listName");

		if(checkParam(res,userName,"Username not set")) return;
		if(checkParam(res,listName,"Listname not set")) return;

	    // Get the listCollection
	    var collection = db.get('listCollection');

	    // To check if we found what we needed in the db
	    var found = false;

	    // Find all lists
	    console.log("Loading request");
	    collection.find({ userName : userName , listName : listName },{},function(e,data){
   			sendResult(res,data);
   			found = true;
    	});
	});

	// Insert a list

	app.post('/api/list/insert', function(req, res){
		console.log("Insert list");

	    // Get our form values. These rely on the "name" attributes
	    var userName = req.param('userName');
	    var listName = req.param('listName');
	    var data = req.param('data');

	    if(checkParam(res,userName,"Username not found")) return;
	    if(checkParam(res,listName,"Listname not found")) return;
	    if(checkParam(res,data,"Data not found")) return;

	    // Set our collection
	    var collection = db.get('listCollection');

	    // Submit to the DB
	    collection.insert({
	        "userName" : userName,
	        "listName" : listName,
	        "data" : data
	    }, function (err, doc) {
	        if (err) {
	        	console.log("Error ! ",err);
	            sendError(res,"Error while trying to insert");
	        }
	        else {
	            sendResult(res,doc);
	        }
	    });

	});

	// Delete a list

	app.delete('/api/list/delete', function(req, res){
		console.log('Delete list');

		var userName = req.param('userName');
		var listName = req.param('listName');

		if(checkParam(res,userName,"Username not found")) return;
		if(checkParam(res,listName,"Listname not found")) return;

		var collection = db.get('listCollection');

		collection.remove({
			"userName" : userName,
			"listName" : listName
		},function(err,doc){
			if(err)
				console.log("error");
			else
				console.log("goooood delete");
		});
	});

	// Update a list

	app.put('/api/list/update', function(req, res){
		console.log('Update list');

	    var userName = req.param('userName');
	    var listName = req.param('listName');
	    var data = req.param('data');

	    if(checkParam(res,userName,"Username not found")) return;
	    if(checkParam(res,listName,"Listname not found")) return;
	    if(checkParam(res,data,"Data not found")) return;

	    var collection = db.get('listCollection');

	    collection.findAndModify({
	    	"userName" : userName,
	    	"listName" : listName
	    },{
	    	"userName" : userName,
	    	"listName" : listName,
	    	"data" : data
	    },function(succes,error){

	    });
	});

};

function sendResult(res,data)
{
	res.send({ correct : true , response : data});
}

function sendError(res,message)
{
	res.send({ correct : false , response : message });
}

function checkParam(res,param,message)
{
	if(!param)
	{
		sendError(res,message);
	}

	return !param;
}