// Array of allowed value for the region

var regionsAllowed = [ 'eune' , 'euw' , 'br' , 'kr' , 'las' , 'na' , 'lan' , 'oce' , 'ru' , 'tr' ];

// Export the function to the app

module.exports = function(app,lolapi,io) {

	// Check the summoner existance
	
	app.get('/api/summoner', function(req, res){
		
		console.log( "Request summoner information" );

		// Get region and summoner name

		var summonerName = req.param( 'summonerName' , "" );
  		var region = req.param( 'region' , "" );

  		// Check the existance of parameters

  		if(!region.length)
  		{
  			res.send({ correct : false , response : 'Error missing region param' });
  			return;
  		}
  		if(!summonerName.length)
  		{
  			res.send({ correct : false , response : 'Error missing summonerName param' });
  			return;
  		}
  		if(regionsAllowed.indexOf(region.toLocaleLowerCase()) < 0)
  		{
  			res.send({ correct : false , response : 'Error region not allowed' });
  			return;
  		}

		// Check if the summoner exists

		lolapi.Summoner.getByName(summonerName, {region:region}, function (error, summoner) {
			console.log( "Summoner name :" + summonerName + " region: " + region );

			if(error)
			{
				console.log( "Error trying to get summoner data" , error );
				res.send({ correct : false , response : "Error trying to get summoner data" });
			}
			else
			{
				console.log( "Succes!" );
				res.send({ correct : true , response : summoner });
			}
		});
	});

	app.get('/api/summoner/id', function(req,res){
		console.log( "Request summoner information by id" );

		var id = req.param( 'id' , "" );
		var region = req.param( 'region' , "" );

		lolapi.Summoner.get(id, {region:region}, function(error,summoner){
			if(error)
			{
				console.log("error");
			}else
			{
				console.log("Succes!");
				res.send({ correct : true , response : summoner });
			}
		});
	});

	app.get('/api/summoner/icon/', function(req, res){
		console.log( "Request summoner icon" );

		// get icon id
		var id = req.param( "id" , "" );

		if(id.length)
		{
			console.log( "Succes !" );
			send({ correct : true , response : lolapi.ProfileIcon.get(summoner.profileIconId)});
		}
		else
		{
			console.log( "Error missing id param" );
			send({ correct : false , response : 'Error missing id param' });
		}
	});

	app.get('/api/summoner/games', function(req, res){
		console.log( "Requesting summoner recent games" );

		var id = req.param( 'id' , "" );
		if(!id.length)
			res.send({ correct : false , response : "Error missing id param" });

		var region = req.param( 'region' , "" );
		if(!region.length)
			res.send({ correct : false , response : "Error missing region" });

		lolapi.Game.getBySummonerId(id, {region:region},function(error,currentGameList){
			console.log( "Summoner id : " , id );
			if(error)
			{
				console.log( "Error trying to get summoner recent games + " , error );
				res.send({ correct : false , response : "Error trying to get summoner recent games" });
			}else
			{
				console.log( "Succes!",currentGameList.games[0].stats);
				res.send({ correct : true , response : currentGameList });
			}
		});
	});

	app.get('/api/champion', function(req,res){
		console.log( "Requesting champion icon path" );

		var options =
		{
			dataById : true,
			champData : "image"
		};

		lolapi.Static.getChampions(options,function(error,path){
			if(error)
			{
				console.log( "Error trying to get champion icon" );
				res.send({ correct : false , response : "Error trying to get champion icon" });
			}else
			{
				//console.log( "Success!",path);
				res.send({ correct : true , response : path });
			}
		});
	});
};