/**
 * Main app
 */

var fs=require('fs');
var express = require('express');  
var path = require('path');
var app = module.exports = express.createServer();

var mongo = require('mongodb');
var monk = require('monk');
var db = monk('localhost:27017/lolTeamAnalyzer');

// Configuration

app.configure(function(){
	app.set('views', __dirname + '/public/views');
	app.set('view engine', 'jade');
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(app.router);
	app.use(express.static(__dirname+'/public'));
});

// Init lolapi module

var options = {
	useRedis: true,  
	// web server config
	//  host: '50.30.35.9',
	//  port: 3259,
	cacheTTL: 7200
	//  no_ready_check: true
};
var lolapi = require('lolapi')('855dd59f-5e86-4958-bc61-80fe2f1843d8', 'euw',options);

// Routes

app.get('/', function(req,res){	
	res.render('index');
});

require('./routes/summoner')(app,lolapi);
require('./routes/list')(app,lolapi,db);

// Start listening

app.listen(8080);
